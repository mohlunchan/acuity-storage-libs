interface RetError {
    ret: string | number
    msg: string
}

export const isRetError = (obj: any): obj is RetError => {
    if (obj !== null && typeof obj === "object") {
        if (Array.isArray(obj)) return false
    } else {
        return false
    }

    return obj.ret !== undefined && obj.msg !== undefined && obj.ret != 0
}