import axios, { AxiosInstance } from 'axios'
import { ApiService as ApiServiceClass } from 'mxw-libs-api-token'
import { isRetError } from './utils'

type MimeTypes = string

export interface StorageOptions {
    serviceUrl: string,
    apiInstance: ApiServiceClass,
}

export interface WriteCredential {
    credential: string,
    filename: string,
    hostUrl: string
    localHostUrl: string,
}

export interface ReadCredential {
    credential: string,
    localCredential: string,
}

export interface CredentialOptions {
    filename: string,
    expiresIn?: number,
}

export interface WriteCredentialOptions extends CredentialOptions {
    callback?: {
        url: string,
        data?: string,
        dataType: MimeTypes
    }
}

export default class Storage {
    private apiInstance: ApiServiceClass;
    private axios: AxiosInstance;

    constructor({ serviceUrl, apiInstance }: StorageOptions) {
        //inject ApiService instance
        this.apiInstance = apiInstance

        this.axios = axios.create({
            baseURL: serviceUrl.split("").reverse().indexOf('/') === 0 ? serviceUrl.slice(0, -1) : serviceUrl,
            timeout: 30000,
        })

        const self = this;

        this.axios.interceptors.request.use(async (config) => {
            const token = await self.apiInstance.getToken()

            // add token to header
            config.headers = {
                Authorization: `Bearer ${token}`,
            }

            return config
        }, (error) => {
            return Promise.reject(error);
        })

        this.axios.interceptors.response.use((response) => {
            // Any status code that lie within the range of 2xx cause this function to trigger
            // Do something with response data
            if (isRetError(response.data)) {
                const { msg }: { msg: string } = response.data
                return Promise.reject(msg)
            }

            return response
        }, (error) => {
            // Any status codes that falls outside the range of 2xx cause this function to trigger
            // Do something with response error
            return Promise.reject(error);
        })
    }

    public async getOssWrite(options: WriteCredentialOptions): Promise<Omit<WriteCredential, "localHostUrl">> {
        try {
            const { data: response }: { data: { ret: string | number, data: WriteCredential } } = await this.axios({
                method: 'post',
                url: '/write',
                data: { ...options },
            })

            const { credential, hostUrl } = response.data

            return {
                credential,
                hostUrl,
                filename: options.filename,
            }
        } catch (error) {
            throw error
        }
    }

    public async getOssRead(options: CredentialOptions): Promise<string> {
        try {
            const { data: response }: { data: { ret: string | number, data: ReadCredential } } = await this.axios({
                method: 'post',
                url: `/read`,
                data: { ...options },
            })

            return response.data.credential
        } catch (error) {
            throw error
        }
    }

    public async deleteOssFile(filename: string): Promise<void> {
        try {
            await this.axios({
                method: 'post',
                url: `/delete`,
                data: { filename },
            })

            return;
        } catch (error) {
            throw error
        }
    }
}