import { isRetError } from "../src/utils";
import { describe, it } from 'mocha'
import { expect } from "chai";

describe('isRetError', () => {
    describe('Argument is valid object', () => {
        it('When argument ret is 0 returns false', () => {
            const obj = { ret: 0, msg: 'nani' }
            expect(isRetError(obj)).equals(false)
        })

        it('When argument ret is not 0 returns true', () => {
            const obj = { ret: 'asdad', msg: 'nani' }
            expect(isRetError(obj)).equals(true)
        })

        it('When argument ret is undefined returns false', () => {
            const obj = { msg: 'nani' }
            expect(isRetError(obj)).equals(false)
        })

        it('When argument msg is undefined & argument ret is 0 returns false', () => {
            const obj = { ret: 0 }
            expect(isRetError(obj)).equals(false)
        })

        it('When argument msg is undefined & argument ret not 0 returns false', () => {
            const obj = { ret: 'nani' }
            expect(isRetError(obj)).equals(false)
        })

        it('When argument is an empty object returns false', () => {
            expect(isRetError({})).equals(false)
        })
    })

    describe('Argument is not valid object ', () => {
        it('Argument is string returns false', () => {
            expect(isRetError('nani')).equals(false)
        })

        it('Argument is array returns false', () => {
            expect(isRetError([])).equals(false)
        })

        it('Argument is number returns false', () => {
            expect(isRetError(12331)).equals(false)
        })

        it('Argument is null returns false', () => {
            expect(isRetError(null)).equals(false)
        })

        it('Argument is undefined returns false', () => {
            expect(isRetError(undefined)).equals(false)
        })
    })
})